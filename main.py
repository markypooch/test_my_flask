from flask import Flask, request #import main Flask class and request object

app = Flask(__name__) #create the Flask app

@app.route('/test_my_flask', methods=['POST'])
def test_my_flask():
    return 'Good Job!'

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")